#include <iostream>
#include <omp.h>
void initializeazaNumerele(bool* const toateNumerele,const unsigned &nrElemente) {
    *toateNumerele = false;
    for (unsigned i = 1; i < nrElemente; i += 1) {
        *(toateNumerele + i) = false;
    }
}
void decToBinary(const long long unsigned &numar, bool *const ret)
{
    long long unsigned i = 0, n = numar;
    while (n > 0) {
        ret[i++] = (n % 2);
        n >>= 1;
    }
}
void decToBinary(long long unsigned&& n, bool* const ret)
{
    long long unsigned i = 0;
    while (n > 0) {
        ret[i++] = (n % 2);
        n >>= 1;
    }
}
const long long unsigned sumaElementelor(bool const* const numere, const long long unsigned &nrElemente) {
    long long unsigned suma = 0;
    for (long long unsigned i = 0; i < nrElemente; i += 1) {
        suma += numere[i] * (i + 1);
    }
    return suma;
}
int main()
{
    const unsigned nrElemente = 25;
    bool numere[nrElemente];
    initializeazaNumerele(numere, nrElemente);
    unsigned cardinalulMultimii = 1;
    const long long unsigned nrIteratii = 1 << nrElemente;
    long long unsigned tmp;
    bool aux;
    unsigned long long i;
    #pragma omp parallel for private(i,numere,tmp,aux) shared(cardinalulMultimii) schedule(guided,nrIteratii>>3)
    for (i = 1; i < nrIteratii; i += 1) {
        decToBinary(i, numere);
        tmp = sumaElementelor(numere, nrElemente);
        tmp %= 5;
        aux = !tmp;
        cardinalulMultimii += aux;
    }
    std::cout << cardinalulMultimii;
    return 0;
}
